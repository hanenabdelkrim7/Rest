<?php

namespace App\Controller;

use App\Entity\User;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use http\Env\Request;
use http\Env\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class RegistrationController extends AbstractController
{ public function _constract(UserRepository $userRepository, UserPasswordEncoderInterface $passwordEncoder, EntityManagerInterface $entityManager)
{ $this->UserRepository = $userRepository ;


}
    /**
     * @Route("/registration", name="registration")
     * @param Request $request
     */
    public function index(Request $request)

    { $username=$request->get('username');
    $password=$request->get('password');
    $user = $this->UserRepository ->FindOneBy(['username'=>$username]);
   if(!is_null($user))
   {
       return $this->view(
           ['message'=>'user is exists'],
           statusCode:Response::HTTP_CONFLICT);

    }
   $user= new User();
   $user->setUsername($username);
   $user->setPassword($this->passwordEncoder->encodepassword($user,$password));

   $this->entityManger->presist($user);
   $this->entityManger->flush();
   return $this->view($user,statusCode:Response::HTTP_CREATED)->setContext(new context())->setGroups(['public']);


    }
}
